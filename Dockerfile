FROM tomcat

RUN apt-get update -y
RUN apt-get upgrade -y
COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/scalatra.war

EXPOSE 8081

CMD ["catalina.sh","run"]